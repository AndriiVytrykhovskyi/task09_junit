import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        MathFactorialTest.class,
        CalculatorTestMockito.class,
        FileCreationTest.class
})
public class SuiteTest {
}
