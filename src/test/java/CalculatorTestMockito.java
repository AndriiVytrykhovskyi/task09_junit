import com.vytrykhovskyi.Calculator;
import com.vytrykhovskyi.ICalculator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorTestMockito {

    @Mock
    ICalculator mcalc;

    @InjectMocks
    Calculator calc = new Calculator(mcalc);

    @Test
    @DisplayName("Test add")
    public void testAdd() {
//        when(calc.plus(10, 20)).thenReturn(30);
//        assertEquals(calc.plus(10, 20), 30);
//        verify(mcalc).plus(10, 20);

        doReturn(15).when(mcalc).plus(10, 5);
        assertEquals(calc.plus(10,5),15);
        verify(mcalc).plus(10,5);
    }

    @Test
    @DisplayName("Test div")
    public void testDiv() {
        when(mcalc.div(30, 3)).thenReturn(10);
        assertEquals(calc.div(30, 3), 10, 0);
        verify(mcalc).div(30, 3);

        ArithmeticException exception = new ArithmeticException ("Div by zero");

        doThrow(exception).when(mcalc).div(30, 0);
        assertEquals(calc.div(30, 0),0);
        verify(mcalc).div(30, 0);
    }
}
