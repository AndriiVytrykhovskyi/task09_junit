import com.vytrykhovskyi.FileCreation;
import com.vytrykhovskyi.MathFactorial;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileCreationTest {
    private static FileCreation fileCreation;

    @BeforeClass
    public static void setup() {
        fileCreation = new FileCreation();
    }

    @Test
    public void read() {
        String testFileName = "D:\\ЕПАМ\\Домашні\\task09_JUnit\\testFile.txt";
        String expected = "my test";
        String result = fileCreation.read(testFileName);
        assertEquals(expected,result);
    }
}
