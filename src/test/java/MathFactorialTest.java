import com.vytrykhovskyi.MathFactorial;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MathFactorialTest {

    private static MathFactorial mathFactorial;
    static final int ZERO = 0;
    protected static final int count = 0;

    @BeforeClass
    public static void setup() {
        mathFactorial = new MathFactorial();
    }

    @AfterClass
    public static void lastTest() {
        System.out.println("Say goodbye " + count);
    }

    @Test
    @DisplayName("Test factorial zero")
    public void factorialZero_resultOne() {
     //   count++;
        assertEquals(1, mathFactorial.factorial(0));
    }

    @Test
    @DisplayName("Test factorial one")
    public void factorialOne_resultOne() {
        assertEquals(1, mathFactorial.factorial(1));
    }

    @Test(expected = NumberFormatException.class)
    @DisplayName("Test factorial exception")
    public void factorialMinus_resultException() {
        mathFactorial.factorial(-2);
    }

    @Test
    @DisplayName("Test factorial plus")
    public void factorialPlus_resultPlus() {
        assertEquals(mathFactorial.factorial(3), 6);
    }

    @Test
    @DisplayName("Higher than zero")
    public void factorialPlus_higherThanZero() {
        assertTrue(mathFactorial.factorial(2) > ZERO);
    }
}