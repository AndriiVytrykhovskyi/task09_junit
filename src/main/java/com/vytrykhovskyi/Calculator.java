package com.vytrykhovskyi;

public class Calculator implements ICalculator {
    private ICalculator icalculator;

    public Calculator(ICalculator iCalculator) {
        this.icalculator = iCalculator;
    }

    @Override
    public int plus(int a, int b) {
        return icalculator.plus(a, b);
    }

    @Override
    public int minus(int a, int b) {
        return icalculator.minus(a, b);
    }

    @Override
    public int div(int a, int b) {
        return icalculator.div(a, b);
    }

    @Override
    public int mult(int a, int b) {
        return icalculator.mult(a, b);
    }

    public int returnFive(){
        return 5;
    }
}
