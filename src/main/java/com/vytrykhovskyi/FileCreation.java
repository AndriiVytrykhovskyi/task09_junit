package com.vytrykhovskyi;

import java.io.*;

public class FileCreation {

    public static String read(String filename) {
        StringBuilder str = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File(filename)));
            String strInput;
            while ((strInput = in.readLine()) != null) {
                str.append(strInput);
                str.append("\n");
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }
}
