package com.vytrykhovskyi;

public interface ICalculator {
    int plus (int a, int b);
    int minus(int a, int b);
    int div(int a, int b);
    int mult(int a, int b);
}
